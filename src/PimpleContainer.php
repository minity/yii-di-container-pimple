<?php
/*
 * This file is part of the minity/yii-di-container-pimple package.
 *
 * (c) Anton Tyutin <anton@tyutin.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\DependencyInjection;

use Exception;
use Pimple\Container;
use Yii;

class PimpleContainer extends ContainerComponent
{
    /**
     * @var Container
     */
    private $pimple;

    public function __construct()
    {
        $this->pimple = new Container();
    }

    public function init()
    {
        $this->pimple['app'] = Yii::app();
        parent::init();
    }

    /**
     * Get service by name
     *
     * @param string $serviceName
     *
     * @return object
     */
    public function get($serviceName)
    {
        return $this->pimple->offsetGet($serviceName);
    }

    /**
     * Check service by name
     *
     * @param string $serviceName
     *
     * @return bool
     */
    public function has($serviceName)
    {
        return $this->pimple->offsetExists($serviceName);
    }

    /**
     * Get container parameter by name
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getParameter($name)
    {
        return $this->pimple->offsetGet($name);
    }

    /**
     * Set container parameter by name
     *
     * @param string $name
     * @param mixed  $value
     */
    public function setParameter($name, $value)
    {
        $this->pimple->offsetSet($name, $value);
    }

    /**
     * Load configuration from file into container
     *
     * @param string $filename
     *
     * @throws ConfigurationLoadException if can't load configuration
     */
    public function load($filename)
    {
        if (!is_file($filename)) {
            throw new ConfigurationLoadException(Yii::t(
                'yii-di-container',
                'Configuration file {filename} is not found',
                array('{filename}' => $filename)
            ));
        }
        $container = $this->pimple;
        $app = Yii::app();
        try {
            include $filename;
        } catch(Exception $e) {
            throw new ConfigurationLoadException(Yii::t(
                'yii-di-container',
                'DI Container configuration error'
            ), 0, $e);
        }
    }
}
