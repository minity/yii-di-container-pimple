<?php
/*
 * This file is part of the minity/yii-di-container-pimple package.
 *
 * (c) Anton Tyutin <anton@tyutin.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Minity\DependencyInjection\Test;

use Minity\DependencyInjection\ContainerAwareInterface;
use Minity\DependencyInjection\ContainerComponentsTrait;
use Minity\DependencyInjection\ContainerInterface;
use Minity\DependencyInjection\PimpleContainer;
use PHPUnit_Framework_TestCase;

class PimpleContainerTest extends PHPUnit_Framework_TestCase
{
    /** @var ApplicationStub */
    static public $app;

    public static function setUpBeforeClass()
    {
        self::$app = new ApplicationStub(array(
            'basePath' => __DIR__,
            'components' => array(
                'comp1' => (object)array('name' => 'comp1'),
                'comp2' => (object)array('name' => 'comp2'),
            )
        ));
    }

    public function setUp()
    {
        self::$app->setComponent('container', new PimpleContainer());
    }

    public function testHasApplicationService()
    {
        $this->assertSame(self::$app, self::$app->getContainer()->get('app'));
    }

    /**
     * @expectedException \Minity\DependencyInjection\ConfigurationLoadException
     */
    public function testLoad_UnexistentFile()
    {
        self::$app->getContainer()->load('unexistent');
    }

    public function testLoad_Good()
    {
        self::$app->getContainer()->load(__DIR__ . '/fixtures/good.php');
        self::$app->params['test'] = 'test value';
        $this->assertEquals('test value', self::$app->getComponent('comp1'));
    }

    /**
     * @expectedException \Minity\DependencyInjection\ConfigurationLoadException
     */
    public function testLoad_Bad()
    {
        self::$app->getContainer()->load(__DIR__ . '/fixtures/bad.php');
    }
}


class ApplicationStub extends \CWebApplication implements ContainerAwareInterface
{
    use ContainerComponentsTrait;

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return parent::getComponent('container');
    }
}
